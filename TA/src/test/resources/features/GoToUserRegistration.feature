Feature: GoToUserRegistration
# User is able to navigate to user registration page
Background: 
Given I have opened the URl
And I can view the Home portal 

Scenario Outline: 
Given I can see the user options
And I click on "<actionable>"
Then I navigate to the "<targetPage>"

Examples:
|actionable|targetPage|
|Register|basicBootstrapForm|
|Home|email|