package Test;

import org.junit.runner.RunWith;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)

@CucumberOptions(
        features = "src/resources/features",
        plugin = { "pretty", "json:target/Cucumber.json",
       "html:target/cucumber-html-report",
       "de.monochromata.cucumber.report.PrettyReports:target/cucumber"},
       monochrome = true

)       
public class AppTest {
} 

/** 
import org.testng.annotations.DataProvider;
import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        features = "src/test/resources/features",
        plugin = {"json:target/json-report/cucumber.json",
         "html:target/cucumber-reports"
         //"de.monochromata.cucumber.report.PrettyReports:target/pretty-cucumber"
        },
        monochrome = true                                                                                                                                                                                                  
) 
public class AppTest extends AbstractTestNGCucumberTests{

    @Override
    @DataProvider(parallel = true)
    public Object[][] scenarios() {
        return super.scenarios();
    }
}
*/