package Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ReadProperties {
    public static Properties prop = new Properties();
    public static String getReportConfigPath(String propName) throws IOException{
        FileInputStream fis = new FileInputStream("src/resources/properties/Config.properties");
        prop.load(fis);
        String propValue = prop.getProperty(propName);
        if(propValue!= null) return propValue;
        else throw new RuntimeException(propName + " not specified in the Configuration.properties file for the Key:" + propName);		
    }
}