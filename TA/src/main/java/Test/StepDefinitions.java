package Test;

import java.io.IOException;
import java.net.URL;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import io.cucumber.java.After;
import io.cucumber.java.en.*;


public class StepDefinitions {
    WebDriver driver;
    @Given("I have opened the URl")
    public void i_have_opened_the_u_rl() {
        // Write code here that turns the phrase above into concrete actions
        try{
            String url = ReadProperties.getReportConfigPath("env.url");
            System.out.println(url);
            //String chromeDriverPath = ReadProperties.getReportConfigPath("chromedriver.path");
            //System.setProperty("webdriver.chrome.driver", chromeDriverPath);
            DesiredCapabilities cap=DesiredCapabilities.chrome();
            cap.setBrowserName("chrome");
            String huburl ="http://127.0.0.1:54387//wd/hub";
            //driver = new ChromeDriver();
            driver= new RemoteWebDriver(new URL(huburl), cap);
            driver.get(url);
            // save properties to project root folder

        } catch (IOException io) {
            io.printStackTrace();
        }
    }
    @Given("I can view the Home portal")
    public void i_can_view_the_home_portal() {
        // Write code here that turns the phrase above into concrete actions
        try{
        boolean homePageDisplayed=driver.findElement(By.xpath("//h1[contains(text(),'Automation Demo Site')]")).isDisplayed();
        Assert.assertTrue(homePageDisplayed);
        }
        catch (ElementNotVisibleException el) {

        }
        catch (Exception e) {

        }
    }
    @Given("I can see the user options")
    public void i_can_see_the_options()  {
        try{
        Thread.sleep(3000);
        // Write code here that turns the phrase above into concrete actions
        boolean optionsDisplayed=driver.findElement(By.xpath("//*[@id='header']")).isDisplayed();
        Assert.assertTrue(optionsDisplayed);
        }
        catch (ElementNotVisibleException el) {

        }
        catch (Exception e) {

        }

    }
    @Given("I click on {string}")
    public void i_click_on(String string) {
        // Write code here that turns the phrase above into concrete actions
        try {
        String elemxpath = "//*[contains(text(),'" + string + "')]";
        List <WebElement> el = driver.findElements(By.xpath(elemxpath));
        if(el.size()>0) {
            el.get(0).click();
        }else {
            throw new ElementNotVisibleException(elemxpath);
        }}
        catch (ElementNotVisibleException el) {

        }
        catch (Exception e) {

        }

    }

    
    @Then("I navigate to the {string}")
    public void i_navigate_to_the(String string) {
        // Write code here that turns the phrase above into concrete actions
        try{
        Thread.sleep(3000);
        String elemxpath = "//*[contains(@id,'" + string + "')]";
        List <WebElement> el = driver.findElements(By.xpath(elemxpath));
        Assert.assertTrue(el.size()>0);
        } 
        catch (ElementNotVisibleException el) {

        }
        catch (Exception e) {

        }
    }
    @After
    public void closeDriver() {
        driver.quit();
    }
    

}